const http = require('http');
const fs = require('fs')
const url = 'http://jservice.io/api/clues/?category='

for (let i = 1; i < 100; i++) {
    var request = http.get(url + i, function (response) {

        var buffer = "",
            data

        response.on("data", function (chunk) {
            buffer += chunk;
        });
        if (i === 1) {
            response.on("end", function (err) {
                console.log(buffer);
                console.log("\n");
                data = JSON.parse(buffer);
                fs.writeFileSync('categories.json', JSON.stringify(data))
            });
        } else {
            response.on("end", function (err) {

                data = JSON.parse(buffer);
                fs.appendFileSync('categories.json', JSON.stringify(data))
            });
        }
    });
}